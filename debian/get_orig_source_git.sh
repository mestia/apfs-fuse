#!/bin/bash
#This script clones apfs-fuse master branch from github repo
#and creates _orig.tar.gz archive
#It also checks the current version of debianized source and upsteam's version
#in case versions are the same the betaX counter is encreased.
set -e

die() { echo "$*" 1>&2 ; exit 1; }
if [  ! -d 'debian' ]; then
	die "Can't find debian dir, this script support to run as ./debian/get-orig-source"
fi

readonly SOURCE_DATE=$(dpkg-parsechangelog --show-field=Date)

readonly BRANCH='master'
readonly DESTREPO='apfs-fuse.git'
readonly TB='../tarballs'
readonly PKG='apfs-fuse'

readonly REPO='https://github.com/sgan81/apfs-fuse'

pushd $TB > /dev/null 2>&1 || die "couldnt find \"$TB\" dir, exiting"
extract_lzfse () {
	pushd 3rdparty/lzfse >/dev/null 2>&1
	git archive --prefix=lzfse/ -o ../../../lzfse.tar master
	popd >/dev/null 2>&1
}

cleanup () {
	find $1 \( \( -type f -a -name ".git*" \) -o \
		   \( -name "appveyor.yml" \) \
		\) -delete
}

if [ -d "$DESTREPO" ]; then
	pushd $DESTREPO >/dev/null 2>&1
	git pull origin master
	extract_lzfse
else
	git clone $REPO $DESTREPO
	pushd $DESTREPO >/dev/null 2>&1
	git submodule init
	git submodule update
	extract_lzfse
fi
readonly COMMIT=$(git log -n1 --format=format:%h ${BRANCH})
popd >/dev/null 2>&1
popd >/dev/null 2>&1

# TODO: increment nextbeta if new COMMIT
readonly DEBVERS='0.0'
readonly NEXTBETA='1'

readonly ORIG=${PKG}_${DEBVERS}'~beta'${NEXTBETA}'~git'${COMMIT}'.orig.tar'
pushd ${TB}/${DESTREPO} >/dev/null 2>&1
git archive --prefix=${PKG}-${COMMIT}/ -o ../$ORIG ${BRANCH}
pushd ../ >/dev/null 2>&1
# unarchive both in order to merge lzfse and apfs-fuse
tar -xf $ORIG && rm -f $ORIG
tar -xf lzfse.tar && rm -f lzfse.tar
mv lzfse/* ${PKG}-${COMMIT}/3rdparty/lzfse/ && rm -rf lzfse
cleanup ${PKG}-${COMMIT}
# see https://wiki.debian.org/ReproducibleBuilds/TimestampsInTarball
tar --sort=name --numeric-owner --owner=0 --group=0 --mode=go=rX,u+rw,a-s \
 --mtime="${BUILD_DATE}" -c ${PKG}-${COMMIT} | gzip -n > $ORIG.gz \
 && rm -rf ${PKG}-${COMMIT}
popd >/dev/null 2>&1
popd >/dev/null 2>&1
echo -n "New orig.tar.gz: "
ls  ${TB}/$ORIG.gz
